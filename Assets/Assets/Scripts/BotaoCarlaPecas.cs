﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BotaoCarlaPecas : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    AudioSource toque;
    public AudioClip clip;
    public Animator animator;

    void Start()
    {

        toque = GetComponent<AudioSource>();
    }
 
    public void OnPointerEnter(PointerEventData eventData)
    {
        toque.PlayOneShot(clip);
        animator.SetBool("Mouse", true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        animator.SetBool("Mouse", false);
    }
}
