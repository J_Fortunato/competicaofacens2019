﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityStates : MonoBehaviour
{
    public Animator[] _animator;
    public bool B, C, D;
    void Start()
    {
        if (B)
        {
            _animator[0].SetBool("IdleB", true);
            _animator[1].SetBool("IdleB", true);
        }
        else
        {
            _animator[0].SetBool("IdleB", false);
            _animator[1].SetBool("IdleB", false);
        }
        if (C)
        {
            _animator[0].SetBool("IdleC", true);
            _animator[1].SetBool("IdleC", true);
        }
        else
        {
            _animator[0].SetBool("IdleC", false);
            _animator[1].SetBool("IdleC", false);
        }
        if (D)
        {
            _animator[0].SetBool("IdleD", true);
            _animator[1].SetBool("IdleD", true);
        }
        else
        {
            _animator[0].SetBool("IdleD", false);
            _animator[1].SetBool("IdleD", false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
