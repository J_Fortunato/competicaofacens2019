﻿using UnityEngine;

public class Carro : MonoBehaviour
{
    AudioSource _Som;
    private Car _car;
    
    void Start()
    {
        _car = transform.parent.GetComponent<Car>();
        _Som = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(!_car.isStoped)
                _Som.PlayOneShot(_Som.clip);
        }
    }
}