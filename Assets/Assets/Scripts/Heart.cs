﻿using UnityEngine;

public class Heart : MonoBehaviour
{
    AudioSource _Som;
    public AudioClip[] clip;
    Animator _animator;
    private static readonly int Vida = Animator.StringToHash("Vida");

    private int health;
    private static readonly int Completou = Animator.StringToHash("Completou");

    public int Health
    {
        get => health;
        set
        {
            health = value;
            if (health < 50 && health > 10)
            {
                _Som.clip = clip[1];
                _Som.volume = 0.1f;
            }

            else if (health < 10)
            {
                _Som.clip = clip[2];
                _Som.volume = 0.2f;
            }
            else
            {
                _Som.clip = clip[0];
                _Som.volume = 0.05f;
            }
            
            _animator.SetInteger(Vida, health);
        }
    }

    private void Start()
    {
        _Som = GetComponent<AudioSource>();
        _animator = GetComponent<Animator>();
    }

    private void HeartSound()
    {
        _Som.Play();
    }

    public void EndGame()
    {
        _animator.SetTrigger(Completou);
    }
}