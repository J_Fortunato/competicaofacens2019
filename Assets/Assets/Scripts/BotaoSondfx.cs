﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BotaoSondfx : MonoBehaviour,IPointerEnterHandler
{

    AudioSource toque;
    public AudioClip clip;

    void Start()
    {

        toque = GetComponent<AudioSource>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        
        toque.PlayOneShot(clip);
    }

    void Update()
    {

    }
}
