﻿using System;
using System.Collections;
using UnityEngine;

public class ObjectBase : MonoBehaviour
{
    [SerializeField] private Material[] objectMaterials;
    [Space] [SerializeField] private float minShaderRadius;
    [SerializeField] private float maxShaderRadius;
    [Space] [SerializeField] private float appearTime = 5;

    private static readonly int SphereRadius = Shader.PropertyToID("_SphereRadius");
    private static readonly int _WorldPos = Shader.PropertyToID("_WorldPos");

    private bool _fadeIn;

    private void Start()
    {
        for (int i = 0; i < objectMaterials.Length; i++)
        {
            objectMaterials[i].SetVector(_WorldPos, transform.position);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _fadeIn = !_fadeIn;
            StartCoroutine(FadeCoroutine(_fadeIn));
        }
    }

    private IEnumerator FadeCoroutine(bool fadeIn)
    {
        float targetRadius = fadeIn ? minShaderRadius : maxShaderRadius;
        float sphereRadius = fadeIn ? maxShaderRadius : minShaderRadius;

        int multiplier = fadeIn ? -1 : 1;
        
        while (Math.Abs(targetRadius - sphereRadius) > 0.1f)
        {
            sphereRadius += (Time.deltaTime * appearTime) * multiplier;
            for (int i = 0; i < objectMaterials.Length; i++)
            {
                objectMaterials[i].SetFloat(SphereRadius, sphereRadius);
            }

            yield return null;
        }
    }
}