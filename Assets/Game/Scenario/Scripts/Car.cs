﻿using DG.Tweening;
using UnityEngine;

public class Car : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Transform startPoint;
    [SerializeField] private Transform endPoint;

    private Tweener t;
    
    [SerializeField] AudioSource _Som;

    public bool isStoped;
    
    private void Start()
    {
        StartPath(false);
    }

    public void StartPath(bool startpoint)
    {
        isStoped = false;
        
        if(startpoint)
            transform.position = startPoint.position;
        
        t = transform.DOMove(endPoint.position, speed).SetSpeedBased(true).SetEase(Ease.Linear);
        t.onComplete = delegate { StartPath(true); };
    }

    public void Stop()
    {
        if (!isStoped)
        {
            isStoped = true;
            _Som.PlayOneShot(_Som.clip);
            t.Kill();
        }
    }

    public void Respaw()
    {
        t.Kill();
        StartPath(true);
    }
}
