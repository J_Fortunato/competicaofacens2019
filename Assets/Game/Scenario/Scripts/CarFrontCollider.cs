﻿using UnityEngine;

public class CarFrontCollider : MonoBehaviour
{
    [SerializeField] private Car car;
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (car.isStoped)
            {
                car.StartPath(false);
            }
        }
    }
}
