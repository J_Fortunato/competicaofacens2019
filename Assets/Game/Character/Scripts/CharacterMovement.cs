﻿using UnityEngine;

namespace Character
{
    public class CharacterMovement : MonoBehaviour
    {
        [SerializeField] private float walkSpeed = 2;
        [SerializeField] private float runSpeed = 6;
        [Space] [SerializeField] private float turnSmoothTime = .2f;
        [Space] [SerializeField] private float accelerationSmoothTime = 0.1f;
        [SerializeField] private float slowdownSmoothTime = 0.1f;

        private Vector2 _input = Vector2.zero;
        private Vector2 _direction = Vector2.zero;

        private float _turnSmoothVelocity;
        private float _speedSmoothVelocity;
        private float _currentSpeed;

        private Transform _transform;
        private Transform _camera;
        private Rigidbody _rigidbody;

        [HideInInspector] public bool isRunning;
        [HideInInspector] public bool canFall;
        [HideInInspector] public bool canMove;

        private void Start()
        {
            _transform = transform;
            _camera = Camera.main.transform;
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            if (!canMove) return;
            
            _input.x = Input.GetAxisRaw("Horizontal");
            _input.y = Input.GetAxisRaw("Vertical");

            _direction = _input.normalized;

            HandlePosition();

            if (_direction == Vector2.zero) return;

            HandleRotation();
        }


        private void HandleRotation()
        {
            float targetRotation = Mathf.Atan2(_direction.x, _direction.y) * Mathf.Rad2Deg + _camera.eulerAngles.y;
            _transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(_transform.eulerAngles.y, targetRotation,
                                         ref _turnSmoothVelocity, turnSmoothTime);
        }

        private void HandlePosition()
        {
            isRunning = Input.GetKey(KeyCode.LeftShift);
            //float targetSpeed = walkSpeed * _direction.magnitude;
            float targetSpeed = (isRunning ? runSpeed : walkSpeed) * _direction.magnitude;
            bool slowdown = _direction == Vector2.zero;

            _currentSpeed = Mathf.SmoothDamp(_currentSpeed, targetSpeed, ref _speedSmoothVelocity,
                slowdown && canFall ? slowdownSmoothTime : accelerationSmoothTime);

            canFall = _currentSpeed > walkSpeed;

            _rigidbody.position += Time.deltaTime * _currentSpeed * _transform.forward;
            //_transform.Translate(Time.deltaTime * _currentSpeed * _transform.forward, Space.World);

           // float animationSpeedPercent = 1 * _direction.magnitude;
           float animationSpeedPercent = (isRunning ? 1 : 0.2f) * _direction.magnitude;

            CharacterController.instance.Animations.SetMovementPercentValue(animationSpeedPercent,
                slowdown && canFall ? slowdownSmoothTime : accelerationSmoothTime, Time.deltaTime);
        }

        public void Stop()
        {
            _input = Vector2.zero;
            _direction = Vector2.zero;
            _currentSpeed = 0;
        }
    }
}