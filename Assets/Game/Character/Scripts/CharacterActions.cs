﻿using System;
using System.Collections.Generic;
using Character;
using TMPro;
using UnityEngine;

public class CharacterActions : MonoBehaviour
{
    [SerializeField] private float detectionRadius = 2;
    [Space] [SerializeField] private TextMeshProUGUI txtInteractionText;
    [Space] [SerializeField] private GameObject[] uiStars;

    [SerializeField] private GameObject endGame;

    [SerializeField] private List<Interactable> itemsAvailableToInteract = new List<Interactable>();

    public CharacterMovement movement;

    private int starsCollected = 0;

    private void Start()
    {
        starsCollected = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (itemsAvailableToInteract.Count > 0)
            {
                Interact(itemsAvailableToInteract[itemsAvailableToInteract.Count - 1]);
            }
        }
    }

    private void Interact(Interactable item)
    {
        switch (item.type)
        {
            case InteractableType.ObstacleFall:
                //if (CharacterController.instance.Movement.canFall)
                Fall();
                break;
            case InteractableType.Grabable:
                break;
            case InteractableType.Collectable:
                uiStars[starsCollected].SetActive(true);
                starsCollected++;
                itemsAvailableToInteract.Remove(item);
                Destroy(item.gameObject);
                txtInteractionText.enabled = false;

                if (starsCollected == 4)
                {
                    endGame.SetActive(true);
                    CharacterController.instance.GameOver();
                }

                break;
            case InteractableType.Vehicle:
                Car car = item.GetComponent<Car>();
                
                if (!car.isStoped)
                {
                    Fall();
                    car.Stop();
                }
                break;
            case InteractableType.Sign:
                SignMessage.ShowMessage((item as Sign).message);
                break;
        }
    }

    private void EnableInteractionWithItem(Interactable item)
    {
        itemsAvailableToInteract.Add(item);

        switch (item.type)
        {
            case InteractableType.ObstacleFall:
            case InteractableType.Vehicle:
                Interact(item);
                break;
            case InteractableType.Grabable:
            case InteractableType.Collectable:
                Interact(item);
                //ShowIteractionText(item);
                break;
            case InteractableType.Sign:
                ShowIteractionText(item);
                break;
        }
    }

    public void BeGrabbed()
    {
        CharacterController.instance.Animations.SetMovementPercentValue(0, 0, Time.deltaTime);
        CharacterController.instance.Movement.Stop();
        CharacterController.instance.Movement.canMove = false;
    }

    private void Fall()
    {
        CharacterController.instance.Movement.Stop();
        CharacterController.instance.Movement.canMove = false;

        int curHealth = CharacterController.instance.Health.Health;

        if (curHealth - 1 > 0)
            CharacterController.instance.Animations.Fall();

        CharacterController.instance.Health.Health--;
    }

    private void ShowIteractionText(Interactable item)
    {
        switch (item.type)
        {
            case InteractableType.Grabable:
                txtInteractionText.text = "Pressione <b>F</b> para segurar";
                break;
            case InteractableType.Collectable:
                txtInteractionText.text = "Pressione <b>F</b> para coletar";
                break;
            case InteractableType.Sign:
                txtInteractionText.text = "Pressione <b>F</b> para ler a placa";
                break;
        }

        txtInteractionText.enabled = true;
    }

    private void GrabItem(Interactable item)
    {
    }

    private void CollectItem(Interactable item)
    {
    }


    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }

    private void OnTriggerEnter(Collider other)
    {
        Interactable item = other.transform.GetComponent<Interactable>();
        if (item != null)
            EnableInteractionWithItem(item);
    }

    private void OnTriggerExit(Collider other)
    {
        Interactable item = other.transform.GetComponent<Interactable>();
        if (item != null)
        {
            if (itemsAvailableToInteract.Contains(item))
                itemsAvailableToInteract.Remove(item);
        }

        txtInteractionText.enabled = false;
        SignMessage.CloseMessage();
    }
}