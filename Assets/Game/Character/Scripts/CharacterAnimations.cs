﻿using UnityEngine;

namespace Character
{
    public class CharacterAnimations : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private AudioSource passos;
        [SerializeField] private AudioClip[] passoSound;

        private static readonly int Speed = Animator.StringToHash("Speed");
        private static readonly int FallTrigger = Animator.StringToHash("cai");

        public void SetMovementPercentValue(float value, float dampTime, float deltaTime)
        {
            animator.SetFloat(Speed, value, dampTime, deltaTime);
        }

        public void Fall()
        {
            animator.SetFloat(Speed, 0);
            animator.SetTrigger(FallTrigger);
        }

        public void EnableMovementEvent()
        {
            CharacterController.instance.Movement.canMove = true;
        }

        public void PassosSound()
        {
            passos.PlayOneShot(passoSound[Random.Range(0, passoSound.Length)]);
            passos.pitch = Random.Range(0.8F, 1.4F);
        }

    }
}