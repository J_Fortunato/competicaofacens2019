﻿using System;
using System.Collections;
using UnityEngine;

public class CharacterHealth : MonoBehaviour
{
    [SerializeField] private int maxHealth = 3;
    [SerializeField] private Heart heart;
    [Space] [SerializeField] private Transform spawnPoint;

    [HideInInspector] public bool canDie;

    private int health = 3;

    public int Health
    {
        get => health;
        set
        {
            health = Mathf.Clamp(value, 0, maxHealth);
            heart.Health = health;

            if (health <= 0)
            {
                Die();
            }
        }
    }

    private void Start()
    {
        canDie = true;
    }

    public void Die()
    {
        if (!canDie) return;
        canDie = false;

        //Health = 0;
        print("Morreu");
        CharacterController.instance.Movement.canMove = false;
        CharacterController.instance.Movement.Stop();
        CharacterController.instance.Animations.SetMovementPercentValue(0, 0, 0);

        FadeManager.FadeOut(Color.black, .2f, endFadeAction: delegate { StartCoroutine(Respawn()); });
    }

    private IEnumerator Respawn()
    {
        CharacterController.instance.transform.position = spawnPoint.position;
        CharacterController.instance.Animations.Fall();
        Health = 3;
        yield return new WaitForSeconds(1.5f);

        FadeManager.FadeIn(Color.black, duration: 1.5f, endFadeAction: delegate
        {
            canDie = true;
            CharacterController.instance.Movement.canMove = true;
        });
    }

    public void EndGame()
    {
        heart.EndGame();
    }
}