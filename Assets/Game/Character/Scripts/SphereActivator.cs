﻿using System;
using System.Collections;
using UnityEngine;

public class SphereActivator : MonoBehaviour
{
    [SerializeField] private float minRadius;
    [SerializeField] private float maxRadius;
    [SerializeField] private float transitionSpeed;
    [Space]
    [SerializeField] private SphereCollider collider;

    public void Increase(float duration)
    {
        collider.enabled = true;
        StartCoroutine(SphereRadiusCoroutine(duration));
    }

    private IEnumerator SphereRadiusCoroutine(float duration)
    {
        float radius = minRadius;
        while (radius < maxRadius)
        {
            radius += Time.deltaTime * transitionSpeed;

            collider.radius = radius;
            yield return null;
        }

        radius = maxRadius;
        collider.radius = radius;
        
        yield return new WaitForSecondsRealtime(duration);

        while (radius > minRadius)
        {
            radius -= Time.deltaTime * transitionSpeed;

            collider.radius = radius;
            yield return null;
        }

        radius = minRadius;
        collider.radius = radius;

        collider.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("IMG"))
        {
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("IMG"))
        {
            
        }
    }
}
