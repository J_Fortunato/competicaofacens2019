﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class CharacterShaderController : MonoBehaviour
{
    [Header("Shared Settings")] 
    [SerializeField] private float baseRadius;
    [SerializeField] private float transitionSpeed;
    [Space] 
    [SerializeField] private float baseSpherePower;
    [SerializeField] private float transitionPowerSpeed;
    [Space]
    [SerializeField] private float coloredScenarioDuration;

    [Space] [SerializeField] private AudioSource audioSource;
    [Space]
    [SerializeField] private AudioSource[] audiosSources;
    [SerializeField] private float volumeAdicional;
    [Space]
    [SerializeField] private SphereActivator sphereActivator;

    [HideInInspector] public float powerUp = 0;
    
    private Transform _transform;
    private Vector4 _characterPosition = Vector4.zero;

    private static readonly int _WorldPos = Shader.PropertyToID("_WorldPos");
    private static readonly int UncoloredRadius = Shader.PropertyToID("_UncoloredRadius");
    private static readonly int ColoredRadius = Shader.PropertyToID("_ColoredRadius");
    private static readonly int SpherePowerUncolored = Shader.PropertyToID("_SpherePowerUncolored");
    private static readonly int SpherePowerColored = Shader.PropertyToID("_SpherePowerColored");

    private bool _isColored;
    private float _coloredPower = 0;
    private float _startBasePower;

    private void Start()
    {
        _transform = transform;

        Shader.SetGlobalFloat(UncoloredRadius, baseRadius);
        Shader.SetGlobalFloat(ColoredRadius, 0);
        Shader.SetGlobalFloat(SpherePowerUncolored, baseSpherePower);
        Shader.SetGlobalFloat(SpherePowerColored, 0);
    }

    private void Update()
    {
        Vector3 position = _transform.position;

        _characterPosition.x = position.x;
        _characterPosition.y = position.y;
        _characterPosition.z = position.z;
        _characterPosition.w = 0;

        Shader.SetGlobalVector(_WorldPos, _characterPosition);

        if (Input.GetKeyDown(KeyCode.Space) && !_isColored)
        {
            StartCoroutine(ShowColoredScenario());
        }
    }

    private IEnumerator ShowColoredScenario()
    {
        sphereActivator.Increase(coloredScenarioDuration + powerUp);
        audioSource.Play();
        StartCoroutine(IncreaseSpherePower());
        
        _isColored = true;
        float coloredRadius = 0;
        float startBaseRadius = baseRadius;
        while (baseRadius > 0)
        {
            baseRadius -= Time.deltaTime * transitionSpeed;
            coloredRadius += Time.deltaTime * transitionSpeed;

            Shader.SetGlobalFloat(UncoloredRadius, baseRadius);
            Shader.SetGlobalFloat(ColoredRadius, coloredRadius);
            yield return null;
        }

        baseRadius = 0;
        coloredRadius = startBaseRadius;

        Shader.SetGlobalFloat(UncoloredRadius, baseRadius);
        Shader.SetGlobalFloat(ColoredRadius, coloredRadius);

        for (int i = 0; i < audiosSources.Length; i++)
        {
            audiosSources[i].DOFade(audiosSources[i].volume + volumeAdicional, 3);
        }

        yield return new WaitForSecondsRealtime(coloredScenarioDuration + powerUp);

        StartCoroutine(DecreaseSpherePower());
        
        for (int i = 0; i < audiosSources.Length; i++)
        {
            audiosSources[i].DOFade(audiosSources[i].volume - volumeAdicional, 3);
        }
        
        while (coloredRadius > 0)
        {
            baseRadius += Time.deltaTime * transitionSpeed;
            coloredRadius -= Time.deltaTime * transitionSpeed;

            Shader.SetGlobalFloat(UncoloredRadius, baseRadius);
            Shader.SetGlobalFloat(ColoredRadius, coloredRadius);
            yield return null;
        }

        coloredRadius = 0;
        baseRadius = startBaseRadius;

        Shader.SetGlobalFloat(UncoloredRadius, baseRadius);
        Shader.SetGlobalFloat(ColoredRadius, coloredRadius);

        _isColored = false;
    }

    private IEnumerator IncreaseSpherePower()
    {
         _coloredPower = 0;
         _startBasePower = baseSpherePower;
        while (baseSpherePower > 0)
        {
            baseSpherePower -= Time.deltaTime * transitionPowerSpeed;
            _coloredPower += Time.deltaTime * transitionPowerSpeed;

            Shader.SetGlobalFloat(SpherePowerUncolored, baseSpherePower);
            Shader.SetGlobalFloat(SpherePowerColored, _coloredPower);
            yield return null;
        }

        baseSpherePower = 0;
        _coloredPower = _startBasePower;

        Shader.SetGlobalFloat(SpherePowerUncolored, baseSpherePower);
        Shader.SetGlobalFloat(SpherePowerColored, _coloredPower);


    }

    private IEnumerator DecreaseSpherePower()
    {
        while (_coloredPower > 0)
        {
            baseSpherePower += Time.deltaTime * transitionPowerSpeed;
            _coloredPower -= Time.deltaTime * transitionPowerSpeed;

            Shader.SetGlobalFloat(SpherePowerUncolored, baseSpherePower);
            Shader.SetGlobalFloat(SpherePowerColored, _coloredPower);
            yield return null;
        }

        _coloredPower = 0;
        baseSpherePower = _startBasePower;

        Shader.SetGlobalFloat(SpherePowerUncolored, baseSpherePower);
        Shader.SetGlobalFloat(SpherePowerColored, _coloredPower);
    }
}