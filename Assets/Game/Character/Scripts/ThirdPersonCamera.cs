﻿using UnityEngine;

namespace Character
{
    public class ThirdPersonCamera : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private bool lockCursor;
        [Space]
        [SerializeField] private float mouseSensitivity = 10;
        [SerializeField] private float dstFromTraget = 2;
        [SerializeField] private Vector2 picthMinMax = new Vector2(-40, 85);
        [SerializeField] private float rotationSmoothTime = .12f;
        
        private float _yaw;
        private float _pitch;

        private Vector3 _targetRotation;
        private Vector3 _rotationSmoothVelociy;
        private Vector3 _currentRotation;

        private Transform _transform;

        private void Start()
        {
            _transform = transform;
            
            Cursor.lockState = lockCursor ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !lockCursor;
        }

        private void LateUpdate()
        {
            _yaw += Input.GetAxis("Mouse X") * mouseSensitivity;
            _pitch -= Input.GetAxis("Mouse Y") * mouseSensitivity;
            _pitch = Mathf.Clamp(_pitch, picthMinMax.x, picthMinMax.y);
            
            _targetRotation.x = _pitch;
            _targetRotation.y = _yaw;

            _currentRotation = Vector3.SmoothDamp(_currentRotation, _targetRotation, ref _rotationSmoothVelociy,
                rotationSmoothTime);
            
            _transform.eulerAngles = _currentRotation;
            _transform.position = target.position - _transform.forward * dstFromTraget;
        }
    }
}