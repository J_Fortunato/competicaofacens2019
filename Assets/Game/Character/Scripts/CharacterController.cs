﻿using System;
using System.Collections;
using Character;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public static CharacterController instance;

    public CharacterActions Actions;
    public CharacterMovement Movement;
    public CharacterAnimations Animations;
    public CharacterHealth Health;

    public Transform velhaHolder;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        FadeManager.FadeIn(Color.white, endFadeAction: delegate { Movement.canMove = true; });
    }

    public void GameOver()
    {
        StartCoroutine(GameOverCoroutine());
    }

    private IEnumerator GameOverCoroutine()
    {
        Movement.canMove = false;
        Animations.SetMovementPercentValue(0, 0, 0);
        Health.EndGame();
        yield return new WaitForSeconds(3);
        FadeManager.LoadScene(Color.white, "MenuPrincipal", 2);
    }
}