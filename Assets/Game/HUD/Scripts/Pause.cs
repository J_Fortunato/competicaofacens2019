﻿using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameObject pausePanel;

    private bool _isPaused = true;
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseFunction();
        }
    }

    private void PauseFunction()
    {
        _isPaused = !_isPaused;
        
        if (!_isPaused)
        {
            pausePanel.SetActive(true);
            CharacterController.instance.Movement.canMove = false;
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            pausePanel.SetActive(false);
            CharacterController.instance.Movement.canMove = true;
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    public void Menu()
    {
        FadeManager.LoadScene(Color.white, "MenuPrincipal");
    }

    public void Continue()
    {
        PauseFunction();
    }
}
