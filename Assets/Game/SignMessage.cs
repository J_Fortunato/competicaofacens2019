﻿using TMPro;
using UnityEngine;

public class SignMessage : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private TextMeshProUGUI txtMessage;

    private static SignMessage _instance;

    private void Start()
    {
        _instance = this;
    }

    public static void ShowMessage(string message)
    {
        _instance.txtMessage.text = message;
        _instance.canvasGroup.alpha = 1;
    }

    public static void CloseMessage()
    {
        _instance.canvasGroup.alpha = 0;
    }
}
