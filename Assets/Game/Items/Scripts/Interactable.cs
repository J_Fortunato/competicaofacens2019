﻿using UnityEngine;


public enum InteractableType
{
    ObstacleFall,
    Grabable,
    Collectable,
    Vehicle,
    Sign
}

public class Interactable : MonoBehaviour
{
    public InteractableType type;
}