﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    private static FadeManager singleton;

    [SerializeField] private CanvasGroup fadeBackground;

    private AsyncOperation async;
    private Image _fadeImage;

    private void Awake()
    {
        if (singleton == null)
        {
            singleton = this;
            _fadeImage = fadeBackground.GetComponent<Image>();
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public static void FadeIn(Color color, float duration = 1, Action beginFadeAction = null, Action endFadeAction = null)
    {
        singleton._fadeImage.color = color;
        singleton.fadeBackground.alpha = 1;
        singleton.StartCoroutine(singleton.Fade(0, duration, beginFadeAction, endFadeAction));
    }

    public static void FadeOut(Color color, float duration = 1, Action beginFadeAction = null, Action endFadeAction = null)
    {
        singleton._fadeImage.color = color;
        singleton.fadeBackground.alpha = 0;
        singleton.StartCoroutine(singleton.Fade(1, duration, beginFadeAction, endFadeAction));
    }

    public static void LoadScene(Color color, string sceneName, float fadeDuration = 1)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);
        async.allowSceneActivation = false;
        
        FadeOut(color, fadeDuration, endFadeAction: delegate { async.allowSceneActivation = true; });
    }

    public static void LoadScene(Color color, int sceneID, float fadeDuration = 1)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneID);
        async.allowSceneActivation = false;
        
        FadeOut(color, fadeDuration, endFadeAction: delegate { async.allowSceneActivation = true; });
    }

    private IEnumerator Fade(int value, float duration, Action beginFadeAction = null, Action endFadeAction = null)
    {
        beginFadeAction?.Invoke();
        Tweener t = fadeBackground.DOFade(value, duration).SetUpdate(true);
        yield return t.WaitForCompletion();
        endFadeAction?.Invoke();
    }
}
