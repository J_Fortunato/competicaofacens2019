﻿using System;
using System.Collections;
using UnityEngine;

public class Menu : MonoBehaviour
{
    private bool canPress;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        canPress = true;
        Time.timeScale = 1;
        FadeManager.FadeIn(Color.white);
    }

    public void PlayGame()
    {
        if (!canPress) return;

        canPress = false;
        StartCoroutine(PlayGameCoroutine());
    }

    private IEnumerator PlayGameCoroutine()
    {
        yield return new WaitForSeconds(1f);
        FadeManager.LoadScene(Color.white, "Gameplay");
    }
}