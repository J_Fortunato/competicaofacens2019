﻿using DG.Tweening;
using UnityEngine;

public class Velha : MonoBehaviour
{
    [SerializeField] private Animator animator;
    private static readonly int Walk = Animator.StringToHash("Walk");
    private static readonly int Pick = Animator.StringToHash("Pick");

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            CharacterController.instance.Actions.BeGrabbed();
            transform.DOLookAt(other.transform.position, 0.5f).onComplete = () =>
                {
                    animator.SetBool(Walk, true);
                    transform.DOMove(CharacterController.instance.velhaHolder.position, 2f).onComplete = () =>
                    {
                        animator.SetBool(Walk, false);
                        animator.SetTrigger(Pick);
                    };
                };
        }
    }
}
