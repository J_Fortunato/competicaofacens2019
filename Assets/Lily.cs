﻿using DG.Tweening;
using UnityEngine;

public class Lily : MonoBehaviour
{
    [SerializeField] private Transform point_1;
    [SerializeField] private Transform point_2;

    private void Start()
    {
        StartPath();
    }

    private void StartPath()
    {
        transform.DOMove(point_1.position, 20).SetEase(Ease.Linear).onComplete = () =>
        {
            transform.DOLookAt(point_2.position, 1).onComplete = () =>
                {
                    transform.DOMove(point_2.position, 20).SetEase(Ease.Linear).onComplete = () =>
                        {
                            transform.DOLookAt(point_1.position, 1).onComplete = () => { StartPath(); };
                        };
                };
        };
    }
}